Fiverr-poster
=============

Installation
============
1) git clone https://ToGoBananas@bitbucket.org/ToGoBananas/fiverr-poster.git

2) Open console, change directory to project directory(main.py directory)

$ sudo apt install python3.5

3) install requirements 

$ python3.5 -m pip install -r requirements.txt

4) run script with 

$ sudo python3.5 main.py

5) Install phantomJS, for headless browser(you installed that)


PhantomJS installation
======================

Guide available at https://gist.github.com/julionc/7476620
Just change 1.9.8 to 2.1.1


Create new accounts
===================

1) inside envirnoment run 
$ sudo python3.5 create_and_verify_accounts.py

2) restart main.py script to use new accounts


Edit database
=============

1) Edition: Copy main.db to your machine and edit it with sqliteman or similar tool

2) complete removal: just remove main.db from filesystem